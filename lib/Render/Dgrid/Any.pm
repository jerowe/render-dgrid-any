package Render::Dgrid::Any;

use 5.010000;
use strict;
use warnings;

require Exporter;
use Carp;
use Class::MOP;
use Data::Dumper;
use Tie::IxHash;
use Moose::Role;
use List::Uniq qw(:all);

with 'Render::Dgrid';

our @ISA = qw(Exporter);

our $VERSION = '0.01';

has 'stream' => (
    is => 'rw',
);

has 'column_bool' => (
    is => 'rw',
    isa => 'Bool',
    default => 1,
);

sub render_rows_as_hashref{
    my $self = shift;
    my $i;

    croak "Didn't pass a stream object" unless $self->stream;

    if(!$self->columns){
        $self->column_bool(0);
        $self->columns([]);
    }

    if(  eval { $self->stream->can( 'next' ) } ){
        while ( my $block = $self->stream->next ) {
            $self->work($block);
        }
    }
    else{
        foreach my $block (@{$self->stream}){
            $self->work($block);
        }
    }

}

sub work{
    my $self = shift;
    my $block = shift;

    if(ref($block) eq "HASH"){
        if(! $self->column_bool){
            carp "No columns!";
            my @keys = keys %$block;
            map{ push(@{$self->columns}, $_) }@keys;
            @{$self->columns} = uniq(@{$self->columns});
            carp Dumper($self->columns);
        }
        if(! exists $block->{$self->dgid}){
            my $id = join'', map +(0..9,'a'..'z','A'..'Z')[rand(10+26*2)], 1..15;
            $block->{$self->dgid} = $id;
        }
        push(@{$self->dgdata}, $block);
    } 
    else{
        croak "Stream ref is not a hash! Dying!";
    }

}

sub render_header_as_hashref{
    my $self = shift;
    my(%href, @cols);
    tie %href, 'Tie::IxHash';

    @cols = @{$self->get_column_names};

    croak "There are no columns in Dgrid!" unless @cols;

    $self->display_columns(\@cols) if !$self->display_columns;
    @href{@cols} = @{$self->display_columns};

    $self->dgcolumn({});
    $self->dgcolumn->{columns} = \%href;
}

sub get_column_names{
    my $self = shift;
    my($cols);

    #Not sure why it is like this but fine for columns
    return $self->columns if $self->columns;

    croak "There are no columns! You must " unless $self->columns;
}

use namespace::autoclean;
1;

__END__

=head1 NAME

Render::Dgrid::Any - Create an object readable by Dgrid using any object that is an array of flat hashrefs.
This method works for any object that has a next method, and each iteration returns a flat hashref (MangoDB, Data::Table, KiokuDB).

=head1 SYNOPSIS

    package MyTable;
    use Moose;
    extends 'Render';
    with 'Render::Dgrid::Any';

    my $aref = [ {a => 'apple', b => 'boat'}, {a => 'array', b => 'bunny'}, {a => 'aletter', b => 'boat'} ];
    my $dt = MyTable->new();
    $dt->stream($aref);
    
    $dt->columns(['a', 'b']); #The column names can be guessed, but they will come from the hash and may not be in the correct order
    $dt->display_columns(['A', 'B']);
    $dt->process();


=head1 DESCRIPTION

    Parse any array of hashes to an Dgrid ready structure.

=head2 EXPORT

    None by default.

=head1 SEE ALSO

    Render::Dgrid::MongoDB, Render::Dgrid::ResultSet, Render::Dgrid

=head1 AUTHOR

Jillian Rowe, E<lt>jillian.e.rowe@gmail.com<gt>

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2013 by Jillian Rowe

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.16.3 or,
at your option, any later version of Perl 5 you may have available.


=cut
