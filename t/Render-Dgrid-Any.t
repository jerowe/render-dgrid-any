package MyTable;

use strict;
use Data::Dumper;
use Test::More tests => 1;
use Carp;

BEGIN { push @INC, '/home/jillian/perlmodules/Render/lib' };
BEGIN { push @INC, '/home/jillian/perlmodules/Render-Dgrid/lib' };
BEGIN { push @INC, '/home/jillian/perlmodules/Render-Dgrid-Any/lib' };

BEGIN { use_ok('Render::Dgrid') };

BEGIN { use_ok('Render::Dgrid::Any') };

use Moose;
extends 'Render';
with 'Render::Dgrid::Any';

my $dt = MyTable->new();
my $aref = [ {a => 'apple', b => 'boat'}, {a => 'array', b => 'bunny'}, {a => 'aletter', b => 'boat'} ];
$dt->stream($aref);

$dt->columns(['a', 'b']); #The column names can be guessed, but they will come from the hash and may not be in the correct order
$dt->display_columns(['A', 'B']);
$dt->process();

my $data = [
    {
        'a' => 'apple',
        'b' => 'boat',
    },
    {
        'a' => 'array',
        'b' => 'bunny',
    },
    {
        'a' => 'aletter',
        'b' => 'boat',
    }
];
my $cols  = {
    'columns' => {
        'a' => 'A',
        'b' => 'B'
    }
};


my $tmp = $dt->dgdata;
foreach my $t (@$tmp){ delete $t->{'id'}; }
is_deeply($data, $tmp, "Data and Dgdata match!");
is_deeply($cols, $dt->dgcolumn, "Data and columns match!");

$dt = MyTable->new();
$aref = [ {a => 'apple', b => 'boat'}, {a => 'array', b => 'bunny'}, {a => 'aletter', b => 'boat'} ];
$dt->stream($aref);

$dt->process();
$cols  = {
    'columns' => {
        'a' => 'a',
        'b' => 'b'
    }
};
is_deeply($cols, $dt->dgcolumn, "Data and columns match!");
